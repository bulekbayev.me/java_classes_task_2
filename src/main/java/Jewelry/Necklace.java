package Jewelry;

import Stones.*;
import java.util.*;

public class Necklace {

    private String name;
    private double necklacePrice;
    private double necklaceWeightInKarats;
    private GoldenChain chain;
    private ArrayList<Mineral> stonesInTheNecklace;

    public Necklace(String name) {
        this.name = name;
        this.chain = new GoldenChain();
        this.necklacePrice = chain.getPrice();
        this.necklaceWeightInKarats = chain.getWeightInKarats();
        this.stonesInTheNecklace = new ArrayList<>();
    }

    public void addStoneToTheNecklace(Mineral mineral) {
        stonesInTheNecklace.add(mineral);
        necklacePrice += mineral.calculateMineralsFullPrice();
        necklaceWeightInKarats += mineral.getWeightInKarats();
    }

    public void deleteStoneFromNecklace(Mineral mineral) {
        if (stonesInTheNecklace.contains(mineral)) {
            stonesInTheNecklace.remove(mineral);
            necklacePrice -= mineral.calculateMineralsFullPrice();
            necklaceWeightInKarats -= mineral.getWeightInKarats();
        } else {
            System.out.println("There is no such stone in the necklace!");
        }
    }

    public void printTotalWeightInKarats() {
        System.out.println("Necklace's weight in karats is " + String.format("%.1f", getNecklaceWeightInKarats()));
    }

    public void printTotalPrice() {
        System.out.println("Necklace's full price is " + String.format("%.2f", getNecklacePrice()) + "$");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNecklacePrice() {
        return necklacePrice;
    }

    private void setNecklacePrice(double necklacePrice) {
        this.necklacePrice = necklacePrice;
    }

    public double getNecklaceWeightInKarats() {
        return necklaceWeightInKarats;
    }

    private void setNecklaceWeightInKarats(double necklaceWeightInKarats) {
        this.necklaceWeightInKarats = necklaceWeightInKarats;
    }

    public ArrayList<Mineral> getStonesInTheNecklace() {
        return stonesInTheNecklace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Necklace necklace = (Necklace) o;
        return Double.compare(necklace.necklacePrice, necklacePrice) == 0 &&
                Double.compare(necklace.necklaceWeightInKarats, necklaceWeightInKarats) == 0 &&
                Objects.equals(name, necklace.name) &&
                Objects.equals(stonesInTheNecklace, necklace.stonesInTheNecklace);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, necklacePrice, necklaceWeightInKarats, stonesInTheNecklace);
    }

    @Override
    public String toString() {
        return "Necklace \"" + name +
                "\" with a price " + necklacePrice +
                "$ and weight " + necklaceWeightInKarats +
                "K, constructed on a " + chain +
                " with jewelry stones.";
    }
}
