package Jewelry;

public class GoldenChain {

    private double weightInKarats;
    private double price;

    public GoldenChain() {
        this.weightInKarats = 360;
        this.price = 2411;
    }

    public double getWeightInKarats() {
        return weightInKarats;
    }

    public void setWeightInKarats(double weightInKarats) {
        this.weightInKarats = weightInKarats;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "golden chain";
    }
}
