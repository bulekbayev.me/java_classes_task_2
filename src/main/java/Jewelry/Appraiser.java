package Jewelry;

import java.util.Comparator;

public class Appraiser {

    public static void sortStonesInTheNecklaceByValue(Necklace necklace) {
        System.out.print("Sorting jewels by their value...");
        necklace.getStonesInTheNecklace().sort(Comparator.comparing(o -> o.getFullPrice()));
        System.out.print("done." + "\n");
    }

    public static void findStonesInRangeOfClarity(Necklace necklace, int clarityStartValue, int clarityEndValue) {
        System.out.println("Finding jewels in range of clarity...");
        for (int i = 0; i < necklace.getStonesInTheNecklace().size(); i++) {
            if (necklace.getStonesInTheNecklace().get(i).getClarity() >= clarityStartValue && necklace.getStonesInTheNecklace().get(i).getClarity() <= clarityEndValue) {
                System.out.print(necklace.getStonesInTheNecklace().get(i).getName() + ", clarity: " + necklace.getStonesInTheNecklace().get(i).getClarity() + "; ");
            }
        }
        System.out.print("\n");
    }
}
