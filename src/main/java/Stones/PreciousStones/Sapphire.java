package Stones.PreciousStones;

import Stones.*;

public class Sapphire extends Mineral {

    public Sapphire(double weightInKarats, int clarity) {
        super(NamesOfStones.SAPPHIRE, true, weightInKarats, 900.0, clarity);
    }
}
