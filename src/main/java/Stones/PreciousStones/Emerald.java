package Stones.PreciousStones;

import Stones.*;

public class Emerald extends Mineral {

    public Emerald(double weightInKarats, int clarity) {
        super(NamesOfStones.EMERALD, true, weightInKarats, 350.0, clarity);
    }
}
