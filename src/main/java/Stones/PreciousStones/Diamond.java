package Stones.PreciousStones;

import Stones.*;

public class Diamond extends Mineral {

    public Diamond(double weightInKarats, int clarity) {
        super(NamesOfStones.DIAMOND, true, weightInKarats, 1500.0, clarity);
    }
}
