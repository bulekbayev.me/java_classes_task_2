package Stones.PreciousStones;

import Stones.*;

public class Ruby extends Mineral {

    public Ruby(double weightInKarats, int clarity) {
        super(NamesOfStones.RUBY, true, weightInKarats, 800.0, clarity);
    }
}
