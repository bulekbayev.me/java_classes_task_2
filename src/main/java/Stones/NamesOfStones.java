package Stones;

public enum NamesOfStones {

    AMETHYST,
    DIAMOND,
    RUBY,
    JADE,
    EMERALD,
    SAPPHIRE,
    TOPAZ,
    AQUAMARINE
}
