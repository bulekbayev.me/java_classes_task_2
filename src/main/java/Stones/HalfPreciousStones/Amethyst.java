package Stones.HalfPreciousStones;

import Stones.*;

public class Amethyst extends Mineral {

    public Amethyst(double weightInKarats, int clarity) {
        super(NamesOfStones.AMETHYST, false, weightInKarats, 4.0, clarity);
    }
}
