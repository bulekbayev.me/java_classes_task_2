package Stones.HalfPreciousStones;

import Stones.*;

public class Aquamarine extends Mineral {

    public Aquamarine(double weightInKarats, int clarity) {
        super(NamesOfStones.AQUAMARINE, false, weightInKarats, 20.0, clarity);
    }
}
