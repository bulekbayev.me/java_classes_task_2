package Stones.HalfPreciousStones;

import Stones.*;

public class Topaz extends Mineral {

    public Topaz(double weightInKarats, int clarity) {
        super(NamesOfStones.TOPAZ, false, weightInKarats, 70.0, clarity);
    }
}
