package Stones.HalfPreciousStones;

import Stones.*;

public class Jade extends Mineral {

    public Jade(double weightInKarats, int clarity) {
        super(NamesOfStones.JADE, false, weightInKarats, 5.0, clarity);
    }
}
