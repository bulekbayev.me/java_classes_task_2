package Stones;

import java.util.Objects;

public class Mineral {

    private NamesOfStones name;
    private boolean rarity;
    private double weightInKarats;
    private double priceForOneKarat;
    private double fullPrice;
    private int clarity;

    public Mineral(NamesOfStones name, boolean rarity, double weightInKarats, double priceForOneKarat, int clarity) {
        this.name = name;
        this.rarity = rarity;
        this.weightInKarats = checkWeightInKarats(weightInKarats);
        this.priceForOneKarat = checkPriceForOneKarat(priceForOneKarat);
        this.clarity = checkClarityInRange(clarity);
    }

    private double checkWeightInKarats(double weightInKarats) {
        if (weightInKarats <= 0) {
            throw new NumberFormatException("Weight cannot be less than zero!");
        } else {
            return weightInKarats;
        }
    }

    private double checkPriceForOneKarat(double priceForOneKarat) {
        if (priceForOneKarat <= 0) {
            throw new NumberFormatException("Price cannot be less than zero!");
        } else {
            return priceForOneKarat;
        }
    }

    private int checkClarityInRange(int clarity) {
        if (clarity < 0 || clarity > 100) {
            throw new NumberFormatException("Clarity must be in range from 0 to 100!");
        } else {
            return clarity;
        }
    }

    public double calculateMineralsFullPrice() {
        return fullPrice = weightInKarats * priceForOneKarat;
    }

    public NamesOfStones getName() {
        return name;
    }

    public void setName(NamesOfStones name) {
        this.name = name;
    }

    public boolean isRarity() {
        return rarity;
    }

    public void setRarity(boolean rarity) {
        this.rarity = rarity;
    }

    public double getWeightInKarats() {
        return weightInKarats;
    }

    public void setWeightInKarats(double weightInKarats) {
        this.weightInKarats = weightInKarats;
    }

    public double getPriceForOneKarat() {
        return priceForOneKarat;
    }

    public void setPriceForOneKarat(double priceForOneKarat) {
        this.priceForOneKarat = priceForOneKarat;
    }

    public double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public int getClarity() {
        return clarity;
    }

    public void setClarity(int clarity) {
        this.clarity = clarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mineral mineral = (Mineral) o;
        return rarity == mineral.rarity &&
                Double.compare(mineral.weightInKarats, weightInKarats) == 0 &&
                Double.compare(mineral.priceForOneKarat, priceForOneKarat) == 0 &&
                Double.compare(mineral.fullPrice, fullPrice) == 0 &&
                clarity == mineral.clarity &&
                Objects.equals(name, mineral.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rarity, weightInKarats, priceForOneKarat, fullPrice, clarity);
    }

    @Override
    public String toString() {
        return name + " price: " + fullPrice + "$, weight: " + weightInKarats + "K" +
               ", clarity: " + clarity;
    }
}
