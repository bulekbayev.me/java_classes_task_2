import Jewelry.*;
import Stones.*;
import Stones.PreciousStones.*;

public class Solution {

    public static void main(String[] args) {
        Mineral diamond = new Diamond(16, 95);
        Mineral diamond1 = new Diamond(14, 60);
        Mineral ruby = new Ruby(13, 70);
        Mineral sapphire = new Sapphire(7, 47);
        Necklace necklace = new Necklace("Queen's heart");
        necklace.addStoneToTheNecklace(diamond);
        necklace.addStoneToTheNecklace(diamond1);
        necklace.addStoneToTheNecklace(ruby);
        necklace.addStoneToTheNecklace(sapphire);
        System.out.println(necklace.toString());
        System.out.println(necklace.getStonesInTheNecklace());
        Appraiser.sortStonesInTheNecklaceByValue(necklace);
        System.out.println(necklace.getStonesInTheNecklace());
        Appraiser.findStonesInRangeOfClarity(necklace, 40, 69);
    }
}